from django.db import models
from django.utils import timezone

class Doctors(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    specialty = models.CharField(max_length=200)
    image_file = models.FileField('Photo', upload_to='static/images')
    text = models.TextField()
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

class Clinic(models.Model):
    author = models.ForeignKey('auth.User')
    clinic_name = models.CharField(max_length=200)
    clinic_telephone = models.CharField(max_length=50)
    clinic_email = models.CharField(max_length=100)
    clinic_doctors = models.TextField()
    clinic_badge = models.FileField('Clinic Badge', upload_to='static/images/clinic_badge')
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.clinic_name

class Contact(models.Model):
    author = models.ForeignKey('auth.User')
    name = models.CharField(max_length=200)
    subject = models.CharField(max_length=200)
    email_address = models.EmailField(max_length=200)
    message = models.TextField(max_length=200)
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.name