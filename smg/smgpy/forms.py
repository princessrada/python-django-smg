from django import forms

from .models import Doctors, Clinic, Contact

class DoctorForm(forms.ModelForm):

    class Meta:
        model = Doctors
        fields = ('title', 'specialty','image_file', 'text')
        
class ClinicForm(forms.ModelForm):
	
	class Meta:
		model = Clinic
		fields = ('clinic_name', 'clinic_telephone', 'clinic_email', 'clinic_doctors', 'clinic_badge')

class ContactForm(forms.ModelForm):

    class Meta:
        model = Contact
        fields = ('name', 'subject','email_address','message')