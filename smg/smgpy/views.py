from django.shortcuts import render
from .models import Doctors, Clinic, Contact
from django.shortcuts import render, get_object_or_404
from .forms import DoctorForm, ClinicForm, ContactForm
from django.shortcuts import redirect
from django.http import Http404, HttpResponse
from django.http import HttpResponseRedirect
import datetime
from django.conf import settings
from django.core.mail import send_mail



# Create your views here.
def home(index):
	return render(index,'smgpy/home.html')

def show_doctors(request):
	docs = Doctors.objects.all()

	return render(request, 'smgpy/show_doctors.html', {'docs' : docs})

def doctor_detail(request, pk):
    detail = get_object_or_404(Doctors, pk=pk)
    return render(request, 'smgpy/doctor_detail.html', {'detail': detail})

def doctor_new(request):
    form = DoctorForm()
    return render(request, 'smgpy/doctor_edit.html', {'form': form})

def doctor_new(request):
    if request.method == "POST":
        form = DoctorForm(request.POST, request.FILES)
        if form.is_valid():
 
            doctor = form.save(commit=False)
            doctor.author = request.user
            doctor.save()
            return redirect('smgpy.views.doctor_detail', pk=doctor.pk)
    else:
        form = DoctorForm()
    return render(request, 'smgpy/doctor_edit.html', {'form': form})


def doctor_edit(request, pk):
    doctor = get_object_or_404(Doctors, pk=pk)
    if request.method == "POST":
        form = DoctorForm(request.POST, request.FILES, instance=doctor)
        if form.is_valid():
            doctor = form.save(commit=False)
            doctor.author = request.user
            doctor.save()
            return redirect('smgpy.views.doctor_detail', pk=doctor.pk)
    else:
        form = DoctorForm(instance=doctor)
    return render(request, 'smgpy/doctor_edit.html', {'form': form})


def current_datetime(request):
 	now = datetime.datetime.now()
 	html = "<html><body>It is now %s.</body></html>"  % now
 	return HttpResponse(html)

def hours_ahead(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    dt = datetime.datetime.now() + datetime.timedelta(hours=offset)
    html = "<html><body>In %s hour(s), it will be %s.</body></html>" % (offset, dt)
    return HttpResponse(html)


# CLINICS

def show_clinics(request):
    clinics = Clinic.objects.all()
    return render(request, 'smgpy/show_clinics.html', {'clinics' : clinics})

def clinic_detail(request, pk):
    detail = get_object_or_404(Clinic, pk=pk)
    return render(request, 'smgpy/clinic_detail.html', {'detail': detail})

def clinic_edit(request, pk):
    clinic = get_object_or_404(Clinic, pk=pk)
    if request.method == "POST":
        form = ClinicForm(request.POST, instance=clinic)
        if form.is_valid():
            clinic = form.save(commit=False)
            clinic.author = request.user
            clinic.save()
            return redirect('smgpy.views.clinic_detail', pk=clinic.pk)
    else:
        form = ClinicForm(instance=clinic)
    return render(request, 'smgpy/clinic_edit.html', {'form': form})

#Contact Form
def contact_new(request):
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
 
            contact = form.save(commit=False)
            contact.author = request.user
            contact.save()
            data = request.POST.copy()
            name = data['name']
            email_address = data['email_address']

            subject = request.POST.get('subject')
            message = request.POST.get('message')
            sender = '%s <%s>' % (name, email_address)
            send_mail(subject, message, sender, [settings.DEFAULT_CONTACT_EMAIL])
            return redirect('/', pk=contact.pk)
    else:
        form = ContactForm()
    return render(request, 'smgpy/new_email.html', {'form': form})


