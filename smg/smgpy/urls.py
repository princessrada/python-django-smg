from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from . import views
from smgpy.views import current_datetime, hours_ahead


urlpatterns = patterns('',
    url(r'^$', views.home),
    url(r'^clinic/$', views.show_clinics),
    url(r'^clinic/(?P<pk>[0-9]+)/$', views.clinic_detail),
    url(r'^clinic/(?P<pk>[0-9]+)/edit/$', views.clinic_edit, name='clinic_edit'),
    url(r'^doctor/$', views.show_doctors),
    url(r'^doctor/(?P<pk>[0-9]+)/$', views.doctor_detail),
    url(r'^doctor/new/$', views.doctor_new, name='doctor_new'),
    url(r'^doctor/(?P<pk>[0-9]+)/edit/$', views.doctor_edit, name='doctor_edit'),
	
    #Contact
    url(r'^contact/$', views.contact_new, name='contact_new'),

    url(r'^time/$', current_datetime),	
	url(r'^time/plus/(\d{1,2})/$', hours_ahead),
    #Auth
    url(r'^login$', 'django.contrib.auth.views.login', {'template_name': 'smgpy/login.html'}),
    url(r'^logout$', 'django.contrib.auth.views.logout', {'next_page': '/'}),



)

