# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('smgpy', '0015_clinic_clinic_badge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clinic',
            name='clinic_badge',
            field=models.FileField(upload_to=b'static/images/'),
            preserve_default=True,
        ),
    ]
