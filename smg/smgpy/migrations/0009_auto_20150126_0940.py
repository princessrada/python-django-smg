# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.files.storage


class Migration(migrations.Migration):

    dependencies = [
        ('smgpy', '0008_doctors_image_url'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='doctors',
            name='image_url',
        ),
        migrations.AlterField(
            model_name='doctors',
            name='image_file',
            field=models.ImageField(upload_to=b'static/images', storage=django.core.files.storage.FileSystemStorage(location=b''), verbose_name=b'Photo'),
            preserve_default=True,
        ),
    ]
