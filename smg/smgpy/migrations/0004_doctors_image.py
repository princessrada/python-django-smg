# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('smgpy', '0003_doctors_specialty'),
    ]

    operations = [
        migrations.AddField(
            model_name='doctors',
            name='image',
            field=models.ImageField(default=datetime.datetime(2015, 1, 23, 9, 43, 16, 379365, tzinfo=utc), upload_to=b'path/', verbose_name=b'Label'),
            preserve_default=False,
        ),
    ]
