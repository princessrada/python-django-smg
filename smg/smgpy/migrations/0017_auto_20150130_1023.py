# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('smgpy', '0016_auto_20150130_1021'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clinic',
            name='clinic_badge',
            field=models.FileField(upload_to=b'static/images/clinic_badge', verbose_name=b'Clinic Badge'),
            preserve_default=True,
        ),
    ]
