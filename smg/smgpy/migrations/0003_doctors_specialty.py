# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('smgpy', '0002_auto_20150121_1013'),
    ]

    operations = [
        migrations.AddField(
            model_name='doctors',
            name='specialty',
            field=models.CharField(default=datetime.datetime(2015, 1, 22, 10, 2, 14, 80061, tzinfo=utc), max_length=200),
            preserve_default=False,
        ),
    ]
