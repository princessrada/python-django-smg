# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('smgpy', '0005_auto_20150123_1008'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='doctors',
            name='image_url',
        ),
    ]
