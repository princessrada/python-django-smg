# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('smgpy', '0004_doctors_image'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='doctors',
            name='image',
        ),
        migrations.AddField(
            model_name='doctors',
            name='image_file',
            field=models.ImageField(default=datetime.datetime(2015, 1, 23, 10, 7, 40, 296268, tzinfo=utc), upload_to=b'images/', verbose_name=b'Photo'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='doctors',
            name='image_url',
            field=models.URLField(default=datetime.datetime(2015, 1, 23, 10, 8, 0, 555654, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
