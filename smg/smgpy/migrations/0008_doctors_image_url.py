# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('smgpy', '0007_auto_20150123_1023'),
    ]

    operations = [
        migrations.AddField(
            model_name='doctors',
            name='image_url',
            field=models.URLField(default=datetime.datetime(2015, 1, 23, 10, 27, 30, 440847, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
