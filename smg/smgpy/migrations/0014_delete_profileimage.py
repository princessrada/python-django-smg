# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('smgpy', '0013_auto_20150127_0841'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ProfileImage',
        ),
    ]
