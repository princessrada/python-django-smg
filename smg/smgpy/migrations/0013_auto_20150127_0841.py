# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('smgpy', '0012_profileimage'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doctors',
            name='image_file',
            field=models.FileField(upload_to=b'static/images', verbose_name=b'Photo'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profileimage',
            name='image',
            field=models.FileField(upload_to=b'static/images', verbose_name=b'Test Image'),
            preserve_default=True,
        ),
    ]
