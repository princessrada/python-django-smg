# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('smgpy', '0010_clinic'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doctors',
            name='image_file',
            field=models.ImageField(upload_to=b'static/images', verbose_name=b'Photo'),
            preserve_default=True,
        ),
    ]
