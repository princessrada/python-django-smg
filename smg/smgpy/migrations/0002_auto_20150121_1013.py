# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('smgpy', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='doctors',
            old_name='other_specialties',
            new_name='text',
        ),
        migrations.RenameField(
            model_name='doctors',
            old_name='doctor_name',
            new_name='title',
        ),
        migrations.RemoveField(
            model_name='doctors',
            name='biography',
        ),
        migrations.RemoveField(
            model_name='doctors',
            name='languages',
        ),
        migrations.RemoveField(
            model_name='doctors',
            name='place_of_practice',
        ),
        migrations.RemoveField(
            model_name='doctors',
            name='specialty',
        ),
    ]
