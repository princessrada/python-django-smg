# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('smgpy', '0014_delete_profileimage'),
    ]

    operations = [
        migrations.AddField(
            model_name='clinic',
            name='clinic_badge',
            field=models.FileField(default=datetime.datetime(2015, 1, 30, 10, 13, 53, 292630, tzinfo=utc), upload_to=b'static/images/clinic_badge'),
            preserve_default=False,
        ),
    ]
