from django.contrib import admin
from .models import Doctors, Clinic, Contact

admin.site.register(Doctors)
admin.site.register(Clinic)
admin.site.register(Contact)

